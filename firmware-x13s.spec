%global _firmwarepath  /usr/lib/firmware

Name:       firmware-x13s
Version:    20230625
Release:    %autorelease
Summary:    Firmware for the Lenovo Thinkpad X13s

License:    GPL+ and GPLv2+ and MIT and Redistributable, no modification permitted
URL:        http://www.kernel.org/
BuildArch:  noarch

Source0:    https://github.com/ironrobin/x13s-alarm/raw/df22ae8444e40801666c6d3a892dc510d4fe51a2/x13s-firmware/SC8280XP-LENOVO-X13S-tplg.bin
Source1:    https://github.com/ironrobin/x13s-alarm/raw/df22ae8444e40801666c6d3a892dc510d4fe51a2/x13s-firmware/a690_gmu.bin
Source3:    https://github.com/ironrobin/x13s-alarm/raw/df22ae8444e40801666c6d3a892dc510d4fe51a2/x13s-firmware/hpnv21.b8c
Source4:    https://github.com/ironrobin/x13s-alarm/raw/df22ae8444e40801666c6d3a892dc510d4fe51a2/x13s-firmware/qcvss8280.mbn
Source5:    https://github.com/linux-surface/aarch64-firmware/raw/fbdee8dd49b4d36564e50b4806e077fe2fd204cf/firmware/qcom/sc8280xp/battmgr.jsn

%description
Extra firmware for the Lenovo Thinkpad X13s

%prep

%build

%install

mkdir -p %{buildroot}/%{_firmwarepath}

mkdir -p %{buildroot}/%{_firmwarepath}/qca
mkdir -p %{buildroot}/%{_firmwarepath}/qcom
mkdir -p %{buildroot}/%{_firmwarepath}/qcom/sc8280xp
mkdir -p %{buildroot}/%{_firmwarepath}/qcom/sc8280xp/LENOVO/21BX

cp %{SOURCE0} %{buildroot}/%{_firmwarepath}/qcom/sc8280xp/SC8280XP-LENOVO-X13S-tplg.bin
cp %{SOURCE1} %{buildroot}/%{_firmwarepath}/qcom/a690_gmu.bin
cp %{SOURCE3} %{buildroot}/%{_firmwarepath}/qca/hpnv21.b8c
cp %{SOURCE4} %{buildroot}/%{_firmwarepath}/qcom/sc8280xp/LENOVO/21BX/qcvss8280.mbn

%if 0%{?fedora} >= 34 || 0%{?rhel} >= 9
xz --compress %{buildroot}/%{_firmwarepath}/qcom/sc8280xp/SC8280XP-LENOVO-X13S-tplg.bin --check=crc32
xz --compress %{buildroot}/%{_firmwarepath}/qcom/a690_gmu.bin --check=crc32
xz --compress %{buildroot}/%{_firmwarepath}/qca/hpnv21.b8c --check=crc32
xz --compress %{buildroot}/%{_firmwarepath}/qcom/sc8280xp/LENOVO/21BX/qcvss8280.mbn --check=crc32
%endif

# This is already a part of linux-firmware, but is loaded by a userspace application which does not detect xz compressed firmware, so include it again here for now
cp %{SOURCE5} %{buildroot}/%{_firmwarepath}/qcom/sc8280xp/LENOVO/21BX/battmgr.jsn

%files
%{_firmwarepath}/qca
%{_firmwarepath}/qcom
